import React, { Component } from "react";

export default class UserInput extends Component {
  render() {
    const { placeholder, event } = this.props;
    return (
      <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-5">
        <div className="mb-4">
          <label
            className="block text-grey-darker text-sm font-bold mb-2"
            htmlFor="username"
          >
            Username
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight"
            name="username"
            type="text"
            onChange={event}
            placeholder={placeholder}
          />
        </div>
      </form>
    );
  }
}
