import React from "react";

export default props => {
  return <p className="mb-3 tracking-wide bg-grey-lighter">{props.username}</p>;
};
