import React, { Component } from "react";
import UserInput from "./components/UserInput";
import UserOutput from "./components/UserOutput";
import Instructions from "./components/Instructions";
import "./App.css";

class App extends Component {
  state = {
    username: "Aivirth"
  };

  changeState = e => {
    this.setState({
      username: e.target.value
    });
  };

  render() {
    return (
      <div className="App">
        <UserInput placeholder={this.state.username} event={this.changeState} />
        <UserOutput username={this.state.username} />
        <UserOutput username="Jack" />
        <UserOutput username="John" />
        <UserOutput username="Caroline" />
      </div>
    );
  }
}

export default App;
